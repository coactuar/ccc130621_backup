-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 10:42 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CCC130621`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/11 12:08:05', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(2, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/12 12:10:32', '2021-06-12', '2021-06-12', 1, 'Abbott'),
(3, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/12 12:12:01', '2021-06-12', '2021-06-12', 1, 'Abbott'),
(4, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/12 12:13:56', '2021-06-12', '2021-06-12', 1, 'Abbott'),
(5, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/12 12:34:19', '2021-06-12', '2021-06-12', 1, 'Abbott'),
(6, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/12 12:56:13', '2021-06-12', '2021-06-12', 1, 'Abbott'),
(7, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/13 17:36:48', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(8, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/13 17:36:55', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(9, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/06/13 17:55:44', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(10, 'Asha Mahilmaran ', 'drashamahil@gmail.com', 'Chennai', 'Apollo Hospital', NULL, NULL, '2021/06/13 18:00:37', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(11, 'SUNDAR', 'sundarsaivimal@gmail.com', 'CHENNAI ', 'KAUVERY HOSPITAL', NULL, NULL, '2021/06/13 18:06:44', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(12, 'SUNDAR', 'sundarsaivimal@gmail.com', 'CHENNAI', 'KAUVERY HOSPITAL', NULL, NULL, '2021/06/13 18:08:58', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(13, 'Arunkumar R', 'arunkumar.r@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021/06/13 18:16:56', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(14, 'Vijayakumar', 'drvijay1977@yahoo.com', 'Chennai', 'The Madras Medical Mission', NULL, NULL, '2021/06/13 18:17:36', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(15, 'Vijayakumar', 'drvijay1977@yahoo.com', 'Chennai', 'The Madras Medical Mission', NULL, NULL, '2021/06/13 18:22:26', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(16, 'Arun Kalyanasundaram', 'arunksundaram@gmail.com', 'FREMONT', 'PROMED', NULL, NULL, '2021/06/13 18:26:09', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(17, 'Arun Kalyanasundaram', 'arunksundaram@gmail.com', 'FREMONT', 'PROMED', NULL, NULL, '2021/06/13 18:26:58', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(18, 'MANOJ S', 'docmanojs@gmail.com', 'CHENNAI', 'Kauvery Hospital', NULL, NULL, '2021/06/13 18:39:29', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(19, 'S MANOJ', 'docmanojs@gmail.com', 'CHENNAI', ' KAUVERY HOSPITAL', NULL, NULL, '2021/06/13 19:04:27', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(20, 'Roopa', 'roopaashok8185@gmail.com', 'Mysore', 'Test', NULL, NULL, '2021/06/13 19:29:25', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(21, 'Arun Kalyanasundaram', 'arunksundaram@gmail.com', 'BELLEVUE', 'Promed Hospital ', NULL, NULL, '2021/06/13 19:57:23', '2021-06-13', '2021-06-13', 1, 'Abbott'),
(22, 'Arun Kalyanasundaram', 'arunksundaram@gmail.com', 'BELLEVUE', 'Promed Hospital ', NULL, NULL, '2021/06/13 20:12:28', '2021-06-13', '2021-06-13', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'What could be the reason for mid portion deformation of the stent and how to avoid...', '2021-06-13 18:47:32', 'Abbott', 0, 0),
(2, 'ARCHANA ', 'josephtheodore84@gmail.com', 'do open cell design with reduced radial strength contribude to LSD? ', '2021-06-13 18:52:15', 'Abbott', 0, 0),
(3, 'sunilpisharody@yahoo.co.in', 'sunilpisharody@yahoo.co.in', 'whether you want to reassess the LAD lesion  mid and distal segments ?', '2021-06-13 19:16:09', 'Abbott', 0, 0),
(4, 'sunilpisharody@yahoo.co.in', 'sunilpisharody@yahoo.co.in', 'thank you good case', '2021-06-13 19:18:57', 'Abbott', 0, 0),
(5, 'ARCHANA ', 'josephtheodore84@gmail.com', 'was kissing balloon necessary in this case to keep the stent struts  open across  as LCX was the same before and after stenting ?', '2021-06-13 19:20:14', 'Abbott', 0, 0),
(6, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'To Dr.Manoj- In your 1st case at what pressure and pulses the IVL was busted?', '2021-06-13 20:52:34', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-11 12:07:45', '2021-06-11 12:07:45', '2021-06-11 12:07:50', 0, 'Abbott', 'b68bccf3a79ab407452a2881fd8c3018'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-11 12:10:44', '2021-06-11 12:10:44', '2021-06-11 12:10:51', 0, 'Abbott', '807745c438bb3f4e99bb99a35ada9df5'),
(3, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-12 09:36:06', '2021-06-12 09:36:06', '2021-06-12 11:06:06', 0, 'Abbott', '814d68476e1bb8e341cce21ad7ad4b22'),
(4, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'Av', NULL, NULL, '2021-06-12 10:02:19', '2021-06-12 10:02:19', '2021-06-12 11:32:19', 0, 'Abbott', '5efc932133ffc68c33a4b3fdb042b510'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-12 16:05:02', '2021-06-12 16:05:02', '2021-06-12 16:05:09', 0, 'Abbott', '33432c8bb5c3e2f83cde6990464afece'),
(6, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-13 10:55:49', '2021-06-13 10:55:49', '2021-06-13 12:25:49', 0, 'Abbott', '275833e0ae006734af9f2e5a8ca9da72'),
(7, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-13 10:56:19', '2021-06-13 10:56:19', '2021-06-13 12:26:19', 0, 'Abbott', 'a683c891644baa4049fccb07bab0ffc3'),
(8, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'Av', NULL, NULL, '2021-06-13 11:06:43', '2021-06-13 11:06:43', '2021-06-13 12:36:43', 0, 'Abbott', 'f38d6ad801b9c6b3464f2ca0c64115ab'),
(9, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-13 11:14:21', '2021-06-13 11:14:21', '2021-06-13 12:44:21', 0, 'Abbott', 'f3b95546c7e3f16a9be6b8cc92afca51'),
(10, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-06-13 13:13:35', '2021-06-13 13:13:35', '2021-06-13 14:43:35', 0, 'Abbott', 'b783b3c47765aec9df0829e9909707d8'),
(11, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-06-13 13:13:48', '2021-06-13 13:13:48', '2021-06-13 14:43:48', 0, 'Abbott', '90f695f753688d6908a5edb54b12530b'),
(12, 'Nishanth S', 'test@test.com', 'bangalore', 'Bangal', NULL, NULL, '2021-06-13 16:14:34', '2021-06-13 16:14:34', '2021-06-13 16:16:02', 0, 'Abbott', '85076ec058fa220e504bbec70d20780c'),
(13, 'Nishanth S', 'test@test.com', 'bangalore', 'Bangal', NULL, NULL, '2021-06-13 16:16:15', '2021-06-13 16:16:15', '2021-06-13 16:16:44', 0, 'Abbott', 'fe651ff8bfaccede1ddb882957d9c200'),
(14, 'Rith', 'rithinraja.rajendran@abbott.com', 'chennai', 'AV', NULL, NULL, '2021-06-13 17:29:06', '2021-06-13 17:29:06', '2021-06-13 18:59:06', 0, 'Abbott', '4be7c45592eab754f7b15752e5c08e2a'),
(15, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-13 18:08:07', '2021-06-13 18:08:07', '2021-06-13 19:38:07', 0, 'Abbott', '583b2153694458cbb487c6b4e54a3d47'),
(16, 'Vijayakumar', 'drvijay1977@yahoo.com', 'Chennai', 'The Madras Medical Mission', NULL, NULL, '2021-06-13 18:14:06', '2021-06-13 18:14:06', '2021-06-13 19:44:06', 0, 'Abbott', 'aa36a5d181434cb10df5319ec9258b36'),
(17, 'Shantanu Mandal', 'dr2.shantanu@gmail.com', 'Delhi', 'AV', NULL, NULL, '2021-06-13 18:19:19', '2021-06-13 18:19:19', '2021-06-13 18:19:30', 0, 'Abbott', '37e5cf207cb157492d4f897390f4cfba'),
(18, 'Rohit A', 'a.rohit@abbott.com', 'Coimbatore ', 'AV', NULL, NULL, '2021-06-13 18:22:35', '2021-06-13 18:22:35', '2021-06-13 19:52:35', 0, 'Abbott', 'db5c6dc4c9a92cd24bd9d743d92c675d'),
(19, 'Ashok ', 'ashok.r@gmail.com', 'Salem', 'Dharan', NULL, NULL, '2021-06-13 18:23:04', '2021-06-13 18:23:04', '2021-06-13 19:53:04', 0, 'Abbott', '093916a6ebe886b0417c03c8ead654a9'),
(20, 'Aravinthan M', 'aravinthan.muthu@gmail.com', 'Salem', 'Dharan', NULL, NULL, '2021-06-13 18:23:38', '2021-06-13 18:23:38', '2021-06-13 19:53:38', 0, 'Abbott', '6b9fee2d97277b52202a078dd99f9542'),
(21, 'Dr Shantanu Mandal', 'shantanu.mandal@abbott.com', 'Delhi', 'AV', NULL, NULL, '2021-06-13 18:30:01', '2021-06-13 18:30:01', '2021-06-13 20:00:01', 0, 'Abbott', '86097a140a1a2063b16220dc36f48266'),
(22, 'Arunkumar R', 'arunkumar.r@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021-06-13 18:30:39', '2021-06-13 18:30:39', '2021-06-13 20:00:39', 0, 'Abbott', '5daf7b1571ae43dfecd32b10d4219c7c'),
(23, 'Dr Aashish Chopra', 'chorpaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021-06-13 18:32:18', '2021-06-13 18:32:18', '2021-06-13 20:02:18', 0, 'Abbott', '46ccd46b1ad111ce22624c2ae48b0701'),
(24, 'Dr V Nandhakumar', 'nandhacard13@gmail.com', 'Chennai', 'Madras Medical Mission', NULL, NULL, '2021-06-13 18:32:43', '2021-06-13 18:32:43', '2021-06-13 20:02:43', 0, 'Abbott', '9c32c35f6792331578dc3661a8b27298'),
(25, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangalore', NULL, NULL, '2021-06-13 18:32:47', '2021-06-13 18:32:47', '2021-06-13 18:33:28', 0, 'Abbott', '235855c479eb37e82fcf91990f6a9c39'),
(26, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-06-13 18:33:35', '2021-06-13 18:33:35', '2021-06-13 18:33:48', 0, 'Abbott', 'afc4cd2a30ef103c146d585e3273d36a'),
(27, 'Srinivasan N', 'cardiodoc06@yahoo.co.in', 'Chennai', 'MMM', NULL, NULL, '2021-06-13 18:33:53', '2021-06-13 18:33:53', '2021-06-13 20:03:53', 0, 'Abbott', '4cf5667264484e0897384e6a31f786ca'),
(28, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-06-13 18:34:16', '2021-06-13 18:34:16', '2021-06-13 18:37:22', 0, 'Abbott', '6d0150f621798f866a3d9de9818eeae0'),
(29, 'Kumar ', 'kumar.jesu@gmail.com', 'Coimbatore ', 'Manipal Hospital', NULL, NULL, '2021-06-13 18:34:39', '2021-06-13 18:34:39', '2021-06-13 20:04:39', 0, 'Abbott', 'af9a666a82ccf1e30c737706c3737912'),
(30, 'arun', 'arunparidr@gmail.com', 'Chennai ', 'Bh', NULL, NULL, '2021-06-13 18:34:40', '2021-06-13 18:34:40', '2021-06-13 20:04:40', 0, 'Abbott', '5224aeeef784b66cda2c760ef779f9fc'),
(31, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-13 18:34:50', '2021-06-13 18:34:50', '2021-06-13 20:04:50', 0, 'Abbott', '96159a7854fb9aa5d49a678afd564024'),
(32, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-13 18:35:07', '2021-06-13 18:35:07', '2021-06-13 20:05:07', 0, 'Abbott', 'fae332eca802628a1ba4189bc96c1a68'),
(33, 'Dr Dinesh', 'dineshmdcard@yahoo.com', 'Chennai', 'MMM', NULL, NULL, '2021-06-13 18:35:34', '2021-06-13 18:35:34', '2021-06-13 20:05:34', 0, 'Abbott', '1231af784f0f26feeba6726ed12bf128'),
(34, 'Srikumar ', 'sri.kk98@gmail.com', 'Salem ', 'Kauvery Hospirak', NULL, NULL, '2021-06-13 18:35:48', '2021-06-13 18:35:48', '2021-06-13 20:05:48', 0, 'Abbott', '74870cf86bffab67e5b8f9c4185a0617'),
(35, 'Nadiya T', 'nadiya.t28@gmail.com', 'Salem', 'Kauvery Hospital', NULL, NULL, '2021-06-13 18:36:45', '2021-06-13 18:36:45', '2021-06-13 20:06:45', 0, 'Abbott', '58a0bbcb40296d82e503199fa5506be1'),
(36, 'Divya.D', 'div98@yahoo.co.in', 'Coimbatore', 'Kurinji Hospital', NULL, NULL, '2021-06-13 18:37:24', '2021-06-13 18:37:24', '2021-06-13 20:07:24', 0, 'Abbott', 'a126573a6dbe6e9b82dbc3580167bdae'),
(37, 'Dr Vignesh', 'vigneshmbbs05@yahoo.co.in', 'Chennai', 'MMM', NULL, NULL, '2021-06-13 18:37:32', '2021-06-13 18:37:32', '2021-06-13 20:07:32', 0, 'Abbott', 'e93dafc0c59380b8b6b3eee8af3b1357'),
(38, 'Harish', 'harish121277@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021-06-13 18:37:56', '2021-06-13 18:37:56', '2021-06-13 20:07:56', 0, 'Abbott', 'e6186d2f120243a444c0dd4cc9841000'),
(39, 'Santhosh', 'santhoshtechapollo@gmail.com', 'Chennai', 'Apollo Ayanambakkam', NULL, NULL, '2021-06-13 18:38:23', '2021-06-13 18:38:23', '2021-06-13 20:08:23', 0, 'Abbott', 'd63eee3ee7954edfd9e9fdb0d1a58bfd'),
(40, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-06-13 18:39:14', '2021-06-13 18:39:14', '2021-06-13 20:09:14', 0, 'Abbott', '1110a104e865cf9ef44da81428c350f6'),
(41, 'Dr Balavignesh', 'drbalavignesh@hotmail.com', 'Vellore', 'Naruvi', NULL, NULL, '2021-06-13 18:41:37', '2021-06-13 18:41:37', '2021-06-13 20:11:37', 0, 'Abbott', '6fad073511b0e29b0417ebc729068482'),
(42, 'Dr Arvind Yuvaraj', 'drarvindyuvaraj@gmail.com', 'Vellore', 'Naruvi', NULL, NULL, '2021-06-13 18:42:09', '2021-06-13 18:42:09', '2021-06-13 20:12:09', 0, 'Abbott', '232f9c7a605eda78b5be93ccf13bf058'),
(43, 'DR Sharath Babu', 'nmsbabu2002@yahoo.com', 'Vlr', 'CMC', NULL, NULL, '2021-06-13 18:42:36', '2021-06-13 18:42:36', '2021-06-13 20:12:36', 0, 'Abbott', '57064df07c2c6f1cccdd0bc7688d2de7'),
(44, 'Ashok', 'gashokmkumar@gmail.com', 'Chennai', 'Chettinad Hospital ', NULL, NULL, '2021-06-13 18:43:16', '2021-06-13 18:43:16', '2021-06-13 20:13:16', 0, 'Abbott', 'ecca9ef8fb6a24e02c4c1ac08b008bc3'),
(45, 'Giridharan ', 'Girigirias@gmail.con', 'Pondicherry ', 'MGM', NULL, NULL, '2021-06-13 18:44:30', '2021-06-13 18:44:30', '2021-06-13 20:14:30', 0, 'Abbott', 'c06f546d1ad679cd32a5e6d977dff9c9'),
(46, 'Shankar M', 'Shankar.Mee98@gmail.com', 'Salem', 'NGH', NULL, NULL, '2021-06-13 18:46:49', '2021-06-13 18:46:49', '2021-06-13 20:16:49', 0, 'Abbott', '3344189bc5cc6b4e17d044fef75d488b'),
(47, 'Lavanya', 'lavi4321@gmail.com', 'Chennai', 'Apollo Hospital ', NULL, NULL, '2021-06-13 18:47:19', '2021-06-13 18:47:19', '2021-06-13 20:17:19', 0, 'Abbott', '41b91d9092404a4582224b41d5b8e96b'),
(48, 'ARCHANA ', 'josephtheodore84@gmail.com', 'Trichy', 'KAUVERY', NULL, NULL, '2021-06-13 18:48:04', '2021-06-13 18:48:04', '2021-06-13 20:18:04', 0, 'Abbott', '7ec6c86ea738f9d208fd9b5c0ceed53e'),
(49, 'Damodaran B', 'db878@yahoo.com', 'Salem', 'Manipal ', NULL, NULL, '2021-06-13 18:48:05', '2021-06-13 18:48:05', '2021-06-13 20:18:05', 0, 'Abbott', '9c4e55174309072434c7d70cd8f2ef4b'),
(50, 'Raja p', 'raja.pushparaj@gmail.com', 'Coimbatore ', 'GKNM Hospital', NULL, NULL, '2021-06-13 18:49:24', '2021-06-13 18:49:24', '2021-06-13 20:19:24', 0, 'Abbott', 'a9673d4cc5167ba3a5663c9fe7578ac0'),
(51, 'Dr Sanat Kumar Sahoo', 'dr.sanatsahoo@gmail.com', 'CUTTACK', 'Utkal hospital Bhubaneswar', NULL, NULL, '2021-06-13 18:50:30', '2021-06-13 18:50:30', '2021-06-13 20:20:30', 0, 'Abbott', '4a59f622a25a0516d9d7a66618dd4494'),
(52, 'Dr Sanat Kumar Sahoo', 'dr.sanatsahoo@gmail.com', 'CUTTACK', 'Utkal hospital Bhubaneswar', NULL, NULL, '2021-06-13 18:51:43', '2021-06-13 18:51:43', '2021-06-13 20:21:43', 0, 'Abbott', '41dd31d79ba5051ac6dc63de9e6aeab5'),
(53, 'Neelraj C', 'neelkmcc@gmail.com', 'Coimbatore ', 'Muthuâ€™s Hospital', NULL, NULL, '2021-06-13 18:52:26', '2021-06-13 18:52:26', '2021-06-13 20:22:26', 0, 'Abbott', 'a7574a2e23898e519a61cd363a411f86'),
(54, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-06-13 18:58:25', '2021-06-13 18:58:25', '2021-06-13 20:28:25', 0, 'Abbott', '53952c069db0a29e9d542752753d223f'),
(55, 'RAMESH NATARAJAN', 'natarajanramesh79@gmail.com', 'Thiruvananthapuram', 'Kims', NULL, NULL, '2021-06-13 19:00:17', '2021-06-13 19:00:17', '2021-06-13 20:30:17', 0, 'Abbott', 'dd7cfe3fdbd88463822de29601f8db52'),
(56, 'sunilpisharody@yahoo.co.in', 'sunilpisharody@yahoo.co.in', 'perithalmanna', 'EMS hospital', NULL, NULL, '2021-06-13 19:06:15', '2021-06-13 19:06:15', '2021-06-13 20:36:15', 0, 'Abbott', '90b4b220c00d692445d5574a89e84412'),
(57, 'Gowtham', 'rajigowthaman@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-06-13 19:12:20', '2021-06-13 19:12:20', '2021-06-13 20:42:20', 0, 'Abbott', '5fb6b7d83f65fd2a4591cdca8249aae3'),
(58, 'Rufus', 'rufusdemel85@gmail.com', 'Hyderabad ', 'Apollo', NULL, NULL, '2021-06-13 19:15:33', '2021-06-13 19:15:33', '2021-06-13 20:45:33', 0, 'Abbott', '4b72174d1d1b59d348c16a6797c518f6'),
(59, 'Karthikeyan ', 'Karthikeyandm@gmail.com', 'Chennai', 'Madha', NULL, NULL, '2021-06-13 19:20:19', '2021-06-13 19:20:19', '2021-06-13 20:50:19', 0, 'Abbott', '730de76189080ff00fb8abb680c73f5b'),
(60, 'Kasi', 'Kasirajaboopathy@gmail.com', 'Pondicherry ', 'Pondicherry', NULL, NULL, '2021-06-13 19:21:21', '2021-06-13 19:21:21', '2021-06-13 20:51:21', 0, 'Abbott', 'ec003e5cd9cdb24fa4842c4122f3151c'),
(61, 'Roopa', 'roopaashok8185@gmail.com', 'Mysore', 'text', NULL, NULL, '2021-06-13 19:26:12', '2021-06-13 19:26:12', '2021-06-13 20:56:12', 0, 'Abbott', '62164bdd2c475286e51c412b6349106c'),
(62, 'Roopa', 'roopaashok8185@gmail.com', 'Mysore', 'text', NULL, NULL, '2021-06-13 19:26:29', '2021-06-13 19:26:29', '2021-06-13 20:56:29', 0, 'Abbott', 'd60c1bd903b6e19441c03302d7ac43ef'),
(63, 'Roopa', 'roopaashok8185@gmail.com', 'Mysore', 'text', NULL, NULL, '2021-06-13 19:26:45', '2021-06-13 19:26:45', '2021-06-13 20:56:45', 0, 'Abbott', 'b75781c22b1c13d2ba48e98813d60d04'),
(64, 'Shyam Kumar G', 'shyyashob@gmail.com', 'Chennai ', 'Promed Hospital', NULL, NULL, '2021-06-13 19:32:03', '2021-06-13 19:32:03', '2021-06-13 21:02:03', 0, 'Abbott', '60ec63419a4bb77c5bd2a17be49542e7'),
(65, 'roopa', 'roopaashok8185@gmail.com', 'Mysor', 'test', NULL, NULL, '2021-06-13 19:32:27', '2021-06-13 19:32:27', '2021-06-13 19:33:50', 0, 'Abbott', '5f4e0aaed85d06a4364cedd24e739f68'),
(66, 'Harish', 'harish2356@gmail.com', 'Chennai ', 'Hosital', NULL, NULL, '2021-06-13 19:40:09', '2021-06-13 19:40:09', '2021-06-13 21:10:09', 0, 'Abbott', 'a2eb9f92bd0b0b868e1d8887a53479a1'),
(67, 'Sathyamurthy', 'isathya21@gmail.com', 'Chennai', 'Apollo Hospital ', NULL, NULL, '2021-06-13 19:51:25', '2021-06-13 19:51:25', '2021-06-13 21:21:25', 0, 'Abbott', 'fa9e93527bb7ab65d7907b0f7afc476e'),
(68, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-06-13 19:59:16', '2021-06-13 19:59:16', '2021-06-13 21:29:16', 0, 'Abbott', '7dba131260cc046473e2ae50d387e4cd'),
(69, 'Arunkumar R', 'arunkumar.r@abbott.con', 'Chennai', 'Abbott', NULL, NULL, '2021-06-13 20:03:32', '2021-06-13 20:03:32', '2021-06-13 21:33:32', 0, 'Abbott', '81107d275c5c0a5f44dbdc22d5722e1a'),
(70, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-13 20:09:09', '2021-06-13 20:09:09', '2021-06-13 21:07:46', 0, 'Abbott', '701cb0af455142f5a9fafe3dc6d326a2'),
(71, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangal', NULL, NULL, '2021-06-13 20:40:34', '2021-06-13 20:40:34', '2021-06-13 20:40:52', 0, 'Abbott', '2a3760741bebe5dc58242a6774abb03d'),
(72, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-13 20:50:06', '2021-06-13 20:50:06', '2021-06-13 20:51:18', 0, 'Abbott', 'd9b4e69869f7502659679c5f575b98c1'),
(73, 'Arunkumar ', 'arunkumar.r@abbott.com', 'Cgennai', 'Abbott', NULL, NULL, '2021-06-13 20:51:41', '2021-06-13 20:51:41', '2021-06-13 21:07:43', 0, 'Abbott', 'c11898fc9afea10185f6ce47d5c67d5f');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
